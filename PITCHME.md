# SAP S4 Hana

SAP S4 Hana Integration with Pricefx

#HSLIDE

## SAP S4 HANA Integration

- Why?
- S4 & OpenData v2
- OData2 Camel Component
- Integration Templates

#HSLIDE

## Why? 

Pricefx partner [Congnitus Consulting](https://cognitusconsulting.com/) provides SAP S4 Hana as a service + consultancy
They like to change password frequently :-)

#HSLIDE

## S4 & OpenData v2
- [OData2 standard](https://www.odata.org/documentation/odata-version-2-0/)
    - EDM, CRUD, API Versioning, Primitive Data Types
    - Universal clients e.g. [Apache Olingo](https://olingo.apache.org/)
- S4 do not exactly implement OpenData 
    - CSFR
    - Does not sends EDM more than once per session
    - Create Request does not accept "some" nullified fileds 

#HSLIDE

## Entity Domain Model - Example
```
  401 - Cognitus has changed password again :)
```

#HSLIDE

## OData2 Camel Component

- [camel-olingo2](https://camel.apache.org/components/latest/olingo2-component.html)
    - Does not suport PFX connection   
- [pfx-odata2](https://pricefx.atlassian.net/wiki/spaces/IMDEV/pages/1783758962/pfx-odata2+Component)

#HSLIDE

## Integration Templates
Mapping S4 entities to PFX entities
- [Import customers](https://bitbucket.org/pricefx/pricefx-integration-manager-templates/src/prod/templates/import-sap-s4-hana-customers-to-pfx/definition)
- [Import products](https://bitbucket.org/pricefx/pricefx-integration-manager-templates/src/prod/templates/import-sap-s4-hana-products-to-pfx/definition)
- [Import transactions](https://bitbucket.org/pricefx/pricefx-integration-manager-templates/src/prod/templates/import-sap-s4-hana-sales-orders-to-pfx/definition)
- [Export rebate & payout records (unfinished, hard to generify)](https://bitbucket.org/pricefx/pricefx-integration-manager-templates/src/prod/templates/export-pfx-rebates-to-sap-s4-hana/definition)
